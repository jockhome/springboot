/*
package com.edu.usts.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
@EnableAutoConfiguration
@Controller
@ConfigurationProperties(prefix = "book")
public class BookController {
//

//  取自定义属性值
    private String author;

    private String name;

//    使用 ConfigurationProperties 开头，需要设置set、get方法
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ResponseBody
    @RequestMapping("/bookinfo")
    public String showinfo(){
        return author+":"+name;
    }

    public static void main(String[] args) {
        SpringApplication.run(BookController.class,args);
    }


}
*/
