package com.edu.usts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/18
 * Description :
 * Version :1.0
 */
@Controller
public class ExceptionController {
    @RequestMapping("/eShow")
    @ResponseBody
    public String eShow(){
        int a = 5/0;
        return "show";
    }
}
