package com.edu.usts.controller;

import com.edu.usts.pojo.Student;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class JsonController {

//    返回数据对象
    @ResponseBody
    @RequestMapping("showjson")
    public Object show(){
        Student s = new Student();
        s.setAge("11");
        s.setId(1);
        s.setName("我是@Bean注入转换！");
        return s;
    }
}
