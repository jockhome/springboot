package com.edu.usts.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("interceptor")
public class MyInterceptorController {

    @ResponseBody
    @RequestMapping("show")
    public String myInterceptorShow(){

        return "Interceptor Show !";
    }
}
