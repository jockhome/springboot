package com.edu.usts.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.charset.Charset;

/**
 * create by hgz
 * 2019.12.4 21点13分
 */

@Controller
public class IndexController {

//    返回路径为"/ "即访问地址为:http://localhost:8080/
    @RequestMapping("/")
    @ResponseBody
    public String index(){
        return "拦截去测试！";
    }

    //定义消息转换器
//    Spring Boot 默认配置了消息转换器
    @Bean
    public StringHttpMessageConverter stringHttpMessageConverter(){
        StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("GBK"));
        return converter;
    }

}
