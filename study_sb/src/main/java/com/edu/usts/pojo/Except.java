package com.edu.usts.pojo;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/22
 * Description :
 * Version :1.0
 */
public class Except {

    private Object ErrCode;

    private String errorMsg;

    public Except() {
    }

    public Object getErrCode() {
        return ErrCode;
    }

    public void setErrCode(Object errCode) {
        ErrCode = errCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
