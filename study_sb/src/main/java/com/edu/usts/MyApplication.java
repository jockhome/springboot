package com.edu.usts;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

//@EnableAutoConfiguration
//@ComponentScan("com.edu.usts.controller")
@SpringBootApplication
public class MyApplication /*extends WebMvcConfigurerAdapter*/ {
    //    启动springboot项目
    public static void main(String[] args) {
        SpringApplication.run(MyApplication.class,args);
    }

   /* @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        super.configureMessageConverters(converters);
//        创建fastjson消息转换器
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
//        创建fastjson的配置对象
        FastJsonConfig config = new FastJsonConfig();
//        对json数据格式化
        config.setSerializerFeatures(SerializerFeature.PrettyFormat);
//        配置传给转换器
        converter.setFastJsonConfig(config);
//        加入集合
        converters.add(converter);
    }*/
   /*@Bean
    public HttpMessageConverters fastJsonMessageConverter(){
//        创建fastjson消息转换器
       FastJsonHttpMessageConverter convert = new FastJsonHttpMessageConverter();
//        创建fastjson的配置对象
       FastJsonConfig config = new FastJsonConfig();
//        对json数据格式化
       config.setSerializerFeatures(SerializerFeature.PrettyFormat);
//        配置传给转换器
       convert.setFastJsonConfig(config);
       HttpMessageConverter<?> converter = convert;
       return new HttpMessageConverters(converter);
   }*/
}
