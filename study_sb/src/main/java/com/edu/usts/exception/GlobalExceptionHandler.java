package com.edu.usts.exception;

import com.edu.usts.pojo.Except;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Guanzhong Hu
 * Date :2019/12/18
 * Description :全局异常处理器
 * Version :1.0
 */
@ControllerAdvice
public class GlobalExceptionHandler {

//  指名处理那些异常
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object handlerException(Exception e){
        Except except = new Except();
        except.setErrCode(500);
        except.setErrorMsg(e.toString());
        return except;
    }

}
